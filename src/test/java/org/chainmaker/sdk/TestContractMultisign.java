/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package org.chainmaker.sdk;

import org.chainmaker.pb.common.ChainmakerTransaction;
import org.chainmaker.pb.common.ContractOuterClass;
import org.chainmaker.pb.common.Request;
import org.chainmaker.pb.common.ResultOuterClass;
import org.chainmaker.pb.syscontract.ContractManage;
import org.chainmaker.pb.syscontract.MultiSign;
import org.chainmaker.pb.syscontract.SystemContractOuterClass;
import org.chainmaker.sdk.utils.FileUtils;
import org.chainmaker.sdk.utils.SdkUtils;
import org.chainmaker.sdk.utils.UtilsException;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class TestContractMultisign extends TestBase {

    private static final String CONTRACT_NAME = "test";
    private static final String CONTRACT_FILE_PATH = "rust-fact-1.0.0.wasm";

    private static final String INVOKE_CONTRACT_METHOD = "save";
    private static final String TxId = "175350816751652dca1660aee31306774484341b3c354db1ab9f57efbbd3edc3";


    public Map<String, byte[]> initContractParams() throws UtilsException {

        byte[] byteCode = FileUtils.getResourceFileBytes(CONTRACT_FILE_PATH);

        Map<String, byte[]> params = new HashMap<>();
        params.put(MultiSign.MultiReq.Parameter.SYS_CONTRACT_NAME.toString(),
                SystemContractOuterClass.SystemContract.CONTRACT_MANAGE.toString().getBytes());

        params.put(MultiSign.MultiReq.Parameter.SYS_METHOD.toString(),
                ContractManage.ContractManageFunction.INIT_CONTRACT.toString().getBytes());

        params.put(ContractManage.InitContract.Parameter.CONTRACT_NAME.toString(),
                CONTRACT_NAME.getBytes());

        params.put(ContractManage.InitContract.Parameter.CONTRACT_VERSION.toString(),
                "1.0".getBytes());

        params.put(ContractManage.InitContract.Parameter.CONTRACT_BYTECODE.toString(),
                byteCode);

        params.put(ContractManage.InitContract.Parameter.CONTRACT_RUNTIME_TYPE.toString(),
                ContractOuterClass.RuntimeType.WASMER.toString().getBytes());

        return params;
    }

    @Test
    public void testMultiSignReq() {
        ResultOuterClass.TxResponse response = null;
        try {
            Map<String, byte[]> params = initContractParams();
            Request.Payload payload = chainClient.createMultiSignReqPayload(params);
            response = chainClient.multiSignContractReq(payload, rpcCallTimeout, false);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }

        Assert.assertNotNull(response);
    }

    @Test
    public void testMultiSignVote() {
        ResultOuterClass.TxResponse response = null;
        try {
            // TxId为multiSignContractReq的交易id
            ChainmakerTransaction.TransactionInfo tx = chainClient.getTxByTxId(TxId, rpcCallTimeout);
            Request.Payload payload = tx.getTransaction().getPayload();
            Request.EndorsementEntry[] endorsementEntries = SdkUtils.getEndorsers(payload, new User[]{adminUser1});
            response = chainClient.multiSignContractVote(payload, endorsementEntries[0], true, rpcCallTimeout, false);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(response);
    }

    @Test
    public void testMultiSignTrig() {
        ResultOuterClass.TxResponse response = null;
        try {
            // TxId为multiSignContractReq的交易id
            response = chainClient.multiSignContractTrig("17729fb9b3798464caa60a5142215b900d51ad4b934a43ce9ef1f7a8e0c900a0", null, rpcCallTimeout, false);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(response);
    }

    @Test
    public void testMultiSignWithPayer() {
        ResultOuterClass.TxResponse response = null;
        try {
            Map<String, byte[]> params = initContractParams();
            Request.Payload payload = chainClient.createMultiSignReqPayload(params);
            Request.EndorsementEntry payer = SdkUtils.signPayload(payload, adminUser1);

            response = chainClient.multiSignContractReqWithPayer(payload, null, payer, rpcCallTimeout, false);
            // TxId为multiSignContractReq的交易id
            Request.EndorsementEntry[] endorsementEntries = SdkUtils.getEndorsers(payload, new User[]{adminUser1, adminUser2, adminUser3});
            response = chainClient.multiSignContractVoteWithGasLimit(payload, endorsementEntries[0], true, 10000,rpcCallTimeout, false);
            response = chainClient.multiSignContractVoteWithGasLimit(payload, endorsementEntries[1], true, 10000,rpcCallTimeout, false);
            response = chainClient.multiSignContractVoteWithGasLimit(payload, endorsementEntries[2], true, 10000,rpcCallTimeout, false);
            // TxId为multiSignContractReq的交易id
//            response = chainClient.multiSignContractTrigWithPayer(payload.getTxId(), payer, null, rpcCallTimeout, false);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(response);
    }

    @Test
    public void testMultiSignQuery() {
        ResultOuterClass.TxResponse response = null;
        try {
            response = chainClient.multiSignContractQuery("", rpcCallTimeout);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertNotNull(response);
    }

    @Test
    public void testMultiSignQueryWithParams() {
        ResultOuterClass.TxResponse response = null;
        try {
            response = chainClient.multiSignContractQueryWithParams("", new HashMap<>(), rpcCallTimeout);
        } catch (SdkException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        System.out.println(response);
        Assert.assertNotNull(response);
    }

    @Test
    public void testMultiSignByManual() {
        ResultOuterClass.TxResponse response = null;
        try {
            Map<String, byte[]> params = initContractParams();
            // 创建执行 req
            Request.Payload payload = chainClient.createMultiSignReqPayload(params);
            response = chainClient.multiSignContractReq(payload, rpcCallTimeout, true);
            Assert.assertNotNull(response);
            // 进行投票
            Request.EndorsementEntry[] endorsementEntries = SdkUtils.getEndorsers(payload, new User[]{adminUser1, adminUser2, adminUser3});
            for (Request.EndorsementEntry endorsementEntry : endorsementEntries) {
                response = chainClient.multiSignContractVote(payload, endorsementEntry, true, rpcCallTimeout, true);
                Assert.assertNotNull(response);
            }
            // 触发执行多签请求
            response = chainClient.multiSignContractTrig(payload.getTxId(), null, rpcCallTimeout, true);
            Assert.assertNotNull(response);
            // 执行合约调用
            Map<String, byte[]> paramMap = new HashMap<>();
            paramMap.put("time", "1434343000".getBytes());
            paramMap.put("file_hash", "cb7fd709631cdf6e82f071c066fee8c9ec77d3425207d5421e378d142521799f".getBytes());
            paramMap.put("file_name", "test_file".getBytes());
            response = chainClient.invokeContract(CONTRACT_NAME, INVOKE_CONTRACT_METHOD,
                    null, paramMap, rpcCallTimeout, syncResultTimeout);
            Assert.assertNotNull(response);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }


}

/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.model;


/**
 * 基础信息
 */
public class Sysinfo extends BaseModel{

    // 关键k eg: archived_block_height
    private String k;

    // 对应k 的value
    private String v;

    public String getK() {
        return k;
    }

    public void setK(String k) {
        this.k = k;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }
}

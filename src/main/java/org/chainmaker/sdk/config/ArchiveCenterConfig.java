/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.config;

/**
 * 归档中心信息
 */
public class ArchiveCenterConfig {

    // 初始区块hash
    private String chainGenesisHash;

    // 归档中心http地址
    private String archiveCenterHttpUrl;

    //
    private Integer requestSecondLimit;

    private String rpcAddress;

    private Boolean tlsEnable;

    private TlsConfig tls;

    private Integer maxSendMsgSize;

    private Integer maxRecvMsgSize;

    public String getChainGenesisHash() {
        return chainGenesisHash;
    }

    public void setChainGenesisHash(String chainGenesisHash) {
        this.chainGenesisHash = chainGenesisHash;
    }

    public String getArchiveCenterHttpUrl() {
        return archiveCenterHttpUrl;
    }

    public void setArchiveCenterHttpUrl(String archiveCenterHttpUrl) {
        this.archiveCenterHttpUrl = archiveCenterHttpUrl;
    }

    public Integer getRequestSecondLimit() {
        return requestSecondLimit;
    }

    public void setRequestSecondLimit(Integer requestSecondLimit) {
        this.requestSecondLimit = requestSecondLimit;
    }

    public String getRpcAddress() {
        return rpcAddress;
    }

    public void setRpcAddress(String rpcAddress) {
        this.rpcAddress = rpcAddress;
    }

    public Boolean getTlsEnable() {
        return tlsEnable;
    }

    public void setTlsEnable(Boolean tlsEnable) {
        this.tlsEnable = tlsEnable;
    }

    public TlsConfig getTls() {
        return tls;
    }

    public void setTls(TlsConfig tls) {
        this.tls = tls;
    }

    public Integer getMaxSendMsgSize() {
        return maxSendMsgSize;
    }

    public void setMaxSendMsgSize(Integer maxSendMsgSize) {
        this.maxSendMsgSize = maxSendMsgSize;
    }

    public Integer getMaxRecvMsgSize() {
        return maxRecvMsgSize;
    }

    public void setMaxRecvMsgSize(Integer maxRecvMsgSize) {
        this.maxRecvMsgSize = maxRecvMsgSize;
    }

    public void setChain_genesis_hash(String chain_genesis_hash) {
        this.chainGenesisHash = chain_genesis_hash;
    }

    public void setArchive_center_http_url(String archive_center_http_url) {
        this.archiveCenterHttpUrl = archive_center_http_url;
    }
    public void setRequest_second_limit(Integer request_second_limit) {
        this.requestSecondLimit = request_second_limit;
    }

    public void setRpc_address(String rpc_address) {
        this.rpcAddress = rpc_address;
    }

    public void setTls_enable(Boolean tls_enable) {
        this.tlsEnable = tls_enable;
    }

    public void setMax_send_msg_size(Integer max_send_msg_size) {
        this.maxSendMsgSize = max_send_msg_size;
    }

    public void setMax_recv_msg_size(Integer max_recv_msg_size) {
        this.maxRecvMsgSize = max_recv_msg_size;
    }

}

/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.config;

/**
 * 连接池配置
 */
public class ConnPoolConfig {

    /**
     * 最大连接数
     */
    private Integer maxTotal;

    /**
     * 最少空闲连接
     */
    private Integer minIdle;


    /**
     * 最大空闲连接
     */
    private Integer maxIdle;

    /**
     * 对象最小的空闲时间。默认即-1，单位：ms
     * 当空闲的时间大于这个值时，执行移除这个对象操作
     */
    private Integer minEvictableIdleTime;

    /**
     * 对象最小的空闲时间，默认即为1800000(30m)，单位：ms
     * 当对象的空闲时间超过这个值，并且当前空闲对象的数量大于最小空闲数量(minIdle)时，执行移除操作
     */
    private Integer softMinEvictableIdleTime;



    /**
     * 空闲对象检测线程的执行周期，单位毫秒。默认值300000ms（5m） ，-1 表示不启用线程回收资源，单位：ms
     */
    private Integer timeBetweenEvictionRuns;

    /**
     * 当没有空闲连接时，获取连接阻塞等待时间，单位：ms
     */
    private Integer  maxWaitMillis;

    /**
     * 没有空闲连接时，获取连接是否阻塞
     */

    private boolean blockWhenExhausted;

    public ConnPoolConfig() {
        this.maxTotal = 100;
        this.minIdle = 5;
        this.maxIdle = 20;
        this.maxWaitMillis = 11000;
        this.minEvictableIdleTime = -1;
        this.softMinEvictableIdleTime= 1800000;
        this.timeBetweenEvictionRuns =300000;
        this.blockWhenExhausted = true;
    }

    public boolean isBlockWhenExhausted() {
        return blockWhenExhausted;
    }

    public void setBlockWhenExhausted(boolean blockWhenExhausted) {
        this.blockWhenExhausted = blockWhenExhausted;
    }
    public Integer getMaxWaitMillis() {
        return maxWaitMillis;
    }

    public void setMaxWaitMillis(Integer maxWaitMillis) {
        this.maxWaitMillis = maxWaitMillis;
    }


    public Integer getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(Integer maxTotal) {
        this.maxTotal = maxTotal;
    }

    public Integer getMinIdle() {
        return minIdle;
    }

    public void setMinIdle(Integer minIdle) {
        this.minIdle = minIdle;
    }

    public Integer getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(Integer maxIdle) {
        this.maxIdle = maxIdle;
    }

    public Integer getMinEvictableIdleTime() {
        return minEvictableIdleTime;
    }

    public void setMinEvictableIdleTime(Integer minEvictableIdleTime) {
        this.minEvictableIdleTime = minEvictableIdleTime;
    }

    public Integer getSoftMinEvictableIdleTime() {
        return softMinEvictableIdleTime;
    }

    public void setSoftMinEvictableIdleTime(Integer softMinEvictableIdleTime) {
        this.softMinEvictableIdleTime = softMinEvictableIdleTime;
    }

    public Integer getTimeBetweenEvictionRuns() {
        return timeBetweenEvictionRuns;
    }

    public void setTimeBetweenEvictionRuns(Integer timeBetweenEvictionRuns) {
        this.timeBetweenEvictionRuns = timeBetweenEvictionRuns;
    }
}

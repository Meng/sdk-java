/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.archivecenter;

/**
 * 通知接口
 */
public interface Notice {

    // 归档时需要用户手动实现当前方法
    void heightNotice(ProcessMessage processMessage);
}

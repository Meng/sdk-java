/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.archivecenter;

import com.google.gson.Gson;
import com.google.protobuf.Message;
import org.chainmaker.pb.archivecenter.Archivecenter;
import org.chainmaker.pb.common.ChainmakerBlock;
import org.chainmaker.pb.common.ChainmakerTransaction;
import org.chainmaker.pb.config.ChainConfigOuterClass;
import org.chainmaker.sdk.ChainClientException;
import org.chainmaker.sdk.config.ArchiveCenterConfig;
import org.chainmaker.sdk.utils.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;


/**
 * 归档中心http实现
 */
public class ArchiveCenterHttpClient implements ArchiveService {

    private ArchiveCenterConfig archiveCenterConfig;

    private int httpCallTimeout = 10000;

    private static final Logger logger = LoggerFactory.getLogger(ArchiveCenterHttpClient.class);


    public ArchiveCenterHttpClient(ArchiveCenterConfig archiveCenterConfig) {
        this.archiveCenterConfig = archiveCenterConfig;
        if (archiveCenterConfig.getRequestSecondLimit() > 0) {
            this.httpCallTimeout = archiveCenterConfig.getRequestSecondLimit() * 1000;
        }
    }

    public ArchiveCenterConfig getArchiveCenterConfig() {
        return archiveCenterConfig;
    }

    public void setArchiveCenterConfig(ArchiveCenterConfig archiveCenterConfig) {
        this.archiveCenterConfig = archiveCenterConfig;
    }

    @Override
    public ChainmakerTransaction.TransactionInfo getTxByTxId(String txId, long rpcCallTimeout) throws ChainClientException {
        // 先从归档中心取
        Message.Builder builder = ChainmakerTransaction.TransactionInfo.newBuilder();
        builder =  this.httpQueryArchiveCenter(builder, ArchiveCenterApi.GetCommonTransactionByTxId.getValue(), new ArchiveCenterQuery(txId, ArchiveCenterQuery.QueryType.TXID));
        if (builder != null ) {
            return (ChainmakerTransaction.TransactionInfo) builder.build();
        }
        return null;
    }

    @Override
    public ChainmakerTransaction.TransactionInfoWithRWSet getTxWithRWSetByTxId(String txId, long rpcCallTimeout) throws ChainClientException {
        Message.Builder builder = ChainmakerTransaction.TransactionInfoWithRWSet.newBuilder();
        builder =  this.httpQueryArchiveCenter(builder, ArchiveCenterApi.GetCommonTransactionWithRWSetByTxId.getValue(), new ArchiveCenterQuery(txId, ArchiveCenterQuery.QueryType.TXID));
        if (builder != null ) {
            return (ChainmakerTransaction.TransactionInfoWithRWSet) builder.build();
        }
        return null;
    }

    @Override
    public ChainmakerBlock.BlockInfo getBlockByHeight(long blockHeight, boolean withRWSet, long rpcCallTimeout) throws ChainClientException {
        // 先从归档中心取
        Message.Builder builder = ChainmakerBlock.BlockInfo.newBuilder();
        builder =  this.httpQueryArchiveCenter(builder, ArchiveCenterApi.GetBlockWithTxRWSetByHeight.getValue(), new ArchiveCenterQuery(blockHeight));
        if (builder != null ) {
            ChainmakerBlock.BlockInfo blockInfo = (ChainmakerBlock.BlockInfo) builder.build();
            if (withRWSet) {
                return ChainmakerBlock.BlockInfo.newBuilder()
                        .addAllRwsetList(blockInfo.getRwsetListList())
                        .build();
            }
            return blockInfo;
        }
        return null;
    }

    @Override
    public ChainmakerBlock.BlockInfo getBlockByHash(String blockHash, boolean withRWSet, long rpcCallTimeout) throws ChainClientException {
        // 先从归档中心取
        Message.Builder builder = ChainmakerBlock.BlockInfo.newBuilder();
        builder =  this.httpQueryArchiveCenter(builder, ArchiveCenterApi.GetBlockWithTxRWSetByHash.getValue(), new ArchiveCenterQuery(blockHash, ArchiveCenterQuery.QueryType.BLOCKHASH));
        if (builder != null ) {
            ChainmakerBlock.BlockInfo blockInfo = (ChainmakerBlock.BlockInfo) builder.build();
            if (withRWSet) {
                return ChainmakerBlock.BlockInfo.newBuilder()
                        .addAllRwsetList(blockInfo.getRwsetListList())
                        .build();
            }
            return blockInfo;
        }
        return null;
    }

    @Override
    public ChainmakerBlock.BlockInfo getBlockByTxId(String txId, boolean withRWSet, long rpcCallTimeout) throws ChainClientException {
        // 先从归档中心取
        Message.Builder builder = ChainmakerBlock.BlockInfo.newBuilder();
        builder =  this.httpQueryArchiveCenter(builder, ArchiveCenterApi.GetBlockWithTxRWSetByTxId.getValue(), new ArchiveCenterQuery(txId, ArchiveCenterQuery.QueryType.TXID));
        if (builder != null ) {
            ChainmakerBlock.BlockInfo blockInfo = (ChainmakerBlock.BlockInfo) builder.build();
            if (withRWSet) {
                return ChainmakerBlock.BlockInfo.newBuilder()
                        .addAllRwsetList(blockInfo.getRwsetListList())
                        .build();
            }
            return blockInfo;
        }
        return null;
    }

    @Override
    public ChainConfigOuterClass.ChainConfig getChainConfigByBlockHeight(long blockHeight, long rpcCallTimeout) throws ChainClientException {
        // 先从归档中心取
        Message.Builder builder = ChainConfigOuterClass.ChainConfig.newBuilder();
        builder =  this.httpQueryArchiveCenter(builder, ArchiveCenterApi.GetConfigByHeight.getValue(), new ArchiveCenterQuery(blockHeight));
        if (builder != null ) {
            return (ChainConfigOuterClass.ChainConfig) builder.build();
        }
        return null;
    }

    @Override
    public void register(ChainmakerBlock.BlockInfo genesis, long rpcCallTimeout) throws ChainClientException {

    }

    @Override
    public void archiveBlock(ChainmakerBlock.BlockInfo block) {

    }

    @Override
    public void archiveBlocks(BlockIterator blockIterator, Notice notice)  {

    }

    @Override
    public Archivecenter.ArchiveStatusResp getArchivedStatus(long rpcCallTimeout) {
        return null;
    }

    // 访问归档中心返回数据
    public <T> T httpQueryArchiveCenter(T t, String apiUri, ArchiveCenterQuery queryParam)  {
        if (archiveCenterConfig == null || Objects.equals(archiveCenterConfig.getArchiveCenterHttpUrl(), "")) {
            return null;
        }
        String requestUri = archiveCenterConfig.getArchiveCenterHttpUrl() + "/" + apiUri;
        queryParam.setChainGenesisHash(archiveCenterConfig.getChainGenesisHash());
        try {
            Gson gson = new Gson();
            String result = HttpUtils.post(requestUri, gson.toJson(queryParam) ,this.httpCallTimeout, this.httpCallTimeout);
            ArchiveCenterResp<T> resp = new ArchiveCenterResp<>();
            resp = gson.fromJson(result, resp.getClass());
            if (resp.getCode() != null && resp.getCode() > 0 ) {
                logger.error("code: {}", resp.getCode());
                return null;
            }
            if (resp.getData() == null ) {
                logger.error("errmsg: {}", resp.getErrorMsg());
                return null;
            }
            // json to Message.Build
            if (t instanceof Message.Builder) {
                com.google.protobuf.util.JsonFormat.parser().merge(gson.toJson(resp.getData()), (Message.Builder) t);
                return t;
            } else {
                // 直接返回数据
                return resp.getData();
            }

//            return gson.fromJson(resp.getData(), t);
        } catch (Exception e) {
            logger.error("err: {}", e.getMessage());
            return null;
        }
    }

}

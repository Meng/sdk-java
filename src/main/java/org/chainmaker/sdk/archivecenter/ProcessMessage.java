/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.archivecenter;

/**
 * 归档执行通知信息对象
 */
public class ProcessMessage {

    // 归档状态 0-正常 1-失败
    private int code;

    // 归档当前区块高度
    private long currentHeight;

    // 归档总数
    private long total;

    // 归档执行信息（一般只有失败才会有）
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public long getCurrentHeight() {
        return currentHeight;
    }

    public void setCurrentHeight(long currentHeight) {
        this.currentHeight = currentHeight;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProcessMessage(long currentHeight, long total) {
        this.currentHeight = currentHeight;
        this.total = total;
    }

    public ProcessMessage(long currentHeight, String message) {
        this.currentHeight = currentHeight;
        this.message = message;
        this.code = 1;
    }

    public ProcessMessage(String message) {
        this.message = message;
        this.code = 1;
    }

    public ProcessMessage(long currentHeight) {
        this.currentHeight = currentHeight;
    }

    @Override
    public String toString() {
        return "ProcessMessage{" +
                "currentHeight=" + currentHeight +
                ", total=" + total +
                ", message='" + message + '\'' +
                '}';
    }
}

/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.archivecenter;

import com.google.gson.annotations.SerializedName;

/**
 * 归档中心请求参数
 */
public class ArchiveCenterQuery {

    // 区块注册的初始hash
    @SerializedName("chain_genesis_hash")
    private String chainGenesisHash;

    @SerializedName("start")
    private Integer start;

    @SerializedName("end")
    private Integer end;

    // 区块hash
    @SerializedName("block_hash")
    private String blockHash;

    // 区块高度
    @SerializedName("height")
    private Long Height;

    // 交易id
    @SerializedName("tx_id")

    private String txId;

    public String getChainGenesisHash() {
        return chainGenesisHash;
    }

    public void setChainGenesisHash(String chainGenesisHash) {
        this.chainGenesisHash = chainGenesisHash;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getEnd() {
        return end;
    }

    public void setEnd(Integer end) {
        this.end = end;
    }

    public String getBlockHash() {
        return blockHash;
    }

    public void setBlockHash(String blockHash) {
        this.blockHash = blockHash;
    }

    public Long getHeight() {
        return Height;
    }

    public void setHeight(Long height) {
        Height = height;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public ArchiveCenterQuery(Long height) {
        Height = height;
    }

    public ArchiveCenterQuery(String value, QueryType type) {
        if (type == QueryType.TXID) {
            this.txId = value;
        }
        if (type == QueryType.BLOCKHASH) {
            this.blockHash = value;
        }
    }

    public enum QueryType {

        TXID,

        BLOCKHASH;

    }
}

/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package org.chainmaker.sdk.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * http请求
 */
public class HttpUtils {

    private static final Logger logger = LoggerFactory.getLogger(HttpUtils.class);


    private final static String REQUEST_METHOD = "POST";
    private final static String REQUEST_CONTENT_TYPE = "Content-type";
    private final static String REQUEST_CONTENT_TYPE_BODY = "application/json";

    private final static String REQUEST_CONTENT_TYPE_VALUE = "text/plain";
    private final static String REQUEST_CHARSET = "Accept-Charset";
    private final static String REQUEST_CHARSET_VALUE = "UTF-8";
    private final static String REQUEST_CONTENT_LENGTH = "Content-Length";

    /**
     * http get 请求
     * @param u 请求访问地址
     * @param readTimeOut 读超时时间
     * @param connectTimeOut 连接超时时间
     * @return 请求结果
     * @throws IOException io连接异常
     */
    public static String get(String u , int readTimeOut , int connectTimeOut) throws IOException {
        String result = "";
        BufferedReader in = null;
        URL realUrl = new URL(u);
        URLConnection connection = realUrl.openConnection();
        connection.setReadTimeout(readTimeOut);
        connection.setConnectTimeout(connectTimeOut);
        connection.connect();
        in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
        String line;
        while ((line = in.readLine()) != null) {
            result += line;
        }
        if (in != null)in.close();
        return result;
    }

    /**
     * http post 请求
     * @param u 请求访问地址
     * @param str 请求参数
     * @param readTimeOut 读超时时间
     * @param connectTimeOut 连接超时时间
     * @return 请求结果
     * @throws IOException io连接异常
     */
    public static String post(String u, String str,int readTimeOut , int connectTimeOut) throws IOException{
        URL url;
        url = new URL(u);
        HttpURLConnection huc;
        DataOutputStream printout = null;
        huc = (HttpURLConnection) url.openConnection();
        huc.setRequestMethod(REQUEST_METHOD);
        huc.setRequestProperty(REQUEST_CONTENT_TYPE, REQUEST_CONTENT_TYPE_BODY);
//        huc.setRequestProperty(REQUEST_CHARSET, REQUEST_CHARSET_VALUE);
        huc.setRequestProperty(REQUEST_CONTENT_LENGTH,String.valueOf(str.getBytes().length));
        huc.setDoOutput(true);
        huc.setReadTimeout(readTimeOut);
        huc.setConnectTimeout(connectTimeOut);
        printout = new DataOutputStream(huc.getOutputStream());
        printout.write(str.getBytes());
        printout.flush();
        if (printout != null) {
            try {
                printout.close();
            } catch (IOException e) {
                logger.error("http close printout error:", e);
            }
        }
        StringBuffer sb = new StringBuffer();
        String line;
        InputStream is = null;
        BufferedReader br = null;
        try {
            is = huc.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (IOException e) {
            logger.error("http input stream error:", e);
            throw e;
        } finally {
            try {
                if(br!=null){
                    br.close();
                }
                if(is!=null){
                    is.close();
                }
                if(huc!=null){
                    huc.disconnect();
                }
            } catch (IOException e) {
                logger.error("http close input stream error:", e);
                return null;
            }
        }
        return sb.toString();
    }

}

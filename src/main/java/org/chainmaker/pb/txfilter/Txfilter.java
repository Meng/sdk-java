// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: txfilter/txfilter.proto

package org.chainmaker.pb.txfilter;

public final class Txfilter {
  private Txfilter() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface StatOrBuilder extends
      // @@protoc_insertion_point(interface_extends:txfilter.Stat)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <pre>
     * A value greater than 0 indicates a false positive in the tx filter, support for multiple transactions in the future
     * </pre>
     *
     * <code>uint32 fp_count = 1;</code>
     */
    int getFpCount();

    /**
     * <pre>
     * Filter query time
     * </pre>
     *
     * <code>int64 filter_costs = 2;</code>
     */
    long getFilterCosts();

    /**
     * <pre>
     * DB query time
     * </pre>
     *
     * <code>int64 db_costs = 3;</code>
     */
    long getDbCosts();
  }
  /**
   * <pre>
   * Stat contains stat
   * </pre>
   *
   * Protobuf type {@code txfilter.Stat}
   */
  public  static final class Stat extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:txfilter.Stat)
      StatOrBuilder {
  private static final long serialVersionUID = 0L;
    // Use Stat.newBuilder() to construct.
    private Stat(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private Stat() {
    }

    @java.lang.Override
    @SuppressWarnings({"unused"})
    protected java.lang.Object newInstance(
        UnusedPrivateParameter unused) {
      return new Stat();
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return this.unknownFields;
    }
    private Stat(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 8: {

              fpCount_ = input.readUInt32();
              break;
            }
            case 16: {

              filterCosts_ = input.readInt64();
              break;
            }
            case 24: {

              dbCosts_ = input.readInt64();
              break;
            }
            default: {
              if (!parseUnknownField(
                  input, unknownFields, extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return org.chainmaker.pb.txfilter.Txfilter.internal_static_txfilter_Stat_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return org.chainmaker.pb.txfilter.Txfilter.internal_static_txfilter_Stat_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              org.chainmaker.pb.txfilter.Txfilter.Stat.class, org.chainmaker.pb.txfilter.Txfilter.Stat.Builder.class);
    }

    public static final int FP_COUNT_FIELD_NUMBER = 1;
    private int fpCount_;
    /**
     * <pre>
     * A value greater than 0 indicates a false positive in the tx filter, support for multiple transactions in the future
     * </pre>
     *
     * <code>uint32 fp_count = 1;</code>
     */
    public int getFpCount() {
      return fpCount_;
    }

    public static final int FILTER_COSTS_FIELD_NUMBER = 2;
    private long filterCosts_;
    /**
     * <pre>
     * Filter query time
     * </pre>
     *
     * <code>int64 filter_costs = 2;</code>
     */
    public long getFilterCosts() {
      return filterCosts_;
    }

    public static final int DB_COSTS_FIELD_NUMBER = 3;
    private long dbCosts_;
    /**
     * <pre>
     * DB query time
     * </pre>
     *
     * <code>int64 db_costs = 3;</code>
     */
    public long getDbCosts() {
      return dbCosts_;
    }

    private byte memoizedIsInitialized = -1;
    @java.lang.Override
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    @java.lang.Override
    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (fpCount_ != 0) {
        output.writeUInt32(1, fpCount_);
      }
      if (filterCosts_ != 0L) {
        output.writeInt64(2, filterCosts_);
      }
      if (dbCosts_ != 0L) {
        output.writeInt64(3, dbCosts_);
      }
      unknownFields.writeTo(output);
    }

    @java.lang.Override
    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (fpCount_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeUInt32Size(1, fpCount_);
      }
      if (filterCosts_ != 0L) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt64Size(2, filterCosts_);
      }
      if (dbCosts_ != 0L) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt64Size(3, dbCosts_);
      }
      size += unknownFields.getSerializedSize();
      memoizedSize = size;
      return size;
    }

    @java.lang.Override
    public boolean equals(final java.lang.Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof org.chainmaker.pb.txfilter.Txfilter.Stat)) {
        return super.equals(obj);
      }
      org.chainmaker.pb.txfilter.Txfilter.Stat other = (org.chainmaker.pb.txfilter.Txfilter.Stat) obj;

      if (getFpCount()
          != other.getFpCount()) return false;
      if (getFilterCosts()
          != other.getFilterCosts()) return false;
      if (getDbCosts()
          != other.getDbCosts()) return false;
      if (!unknownFields.equals(other.unknownFields)) return false;
      return true;
    }

    @java.lang.Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptor().hashCode();
      hash = (37 * hash) + FP_COUNT_FIELD_NUMBER;
      hash = (53 * hash) + getFpCount();
      hash = (37 * hash) + FILTER_COSTS_FIELD_NUMBER;
      hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
          getFilterCosts());
      hash = (37 * hash) + DB_COSTS_FIELD_NUMBER;
      hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
          getDbCosts());
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static org.chainmaker.pb.txfilter.Txfilter.Stat parseFrom(
        java.nio.ByteBuffer data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static org.chainmaker.pb.txfilter.Txfilter.Stat parseFrom(
        java.nio.ByteBuffer data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static org.chainmaker.pb.txfilter.Txfilter.Stat parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static org.chainmaker.pb.txfilter.Txfilter.Stat parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static org.chainmaker.pb.txfilter.Txfilter.Stat parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static org.chainmaker.pb.txfilter.Txfilter.Stat parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static org.chainmaker.pb.txfilter.Txfilter.Stat parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static org.chainmaker.pb.txfilter.Txfilter.Stat parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static org.chainmaker.pb.txfilter.Txfilter.Stat parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static org.chainmaker.pb.txfilter.Txfilter.Stat parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static org.chainmaker.pb.txfilter.Txfilter.Stat parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static org.chainmaker.pb.txfilter.Txfilter.Stat parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    @java.lang.Override
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(org.chainmaker.pb.txfilter.Txfilter.Stat prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    @java.lang.Override
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * <pre>
     * Stat contains stat
     * </pre>
     *
     * Protobuf type {@code txfilter.Stat}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:txfilter.Stat)
        org.chainmaker.pb.txfilter.Txfilter.StatOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return org.chainmaker.pb.txfilter.Txfilter.internal_static_txfilter_Stat_descriptor;
      }

      @java.lang.Override
      protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return org.chainmaker.pb.txfilter.Txfilter.internal_static_txfilter_Stat_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                org.chainmaker.pb.txfilter.Txfilter.Stat.class, org.chainmaker.pb.txfilter.Txfilter.Stat.Builder.class);
      }

      // Construct using org.chainmaker.pb.txfilter.Txfilter.Stat.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      @java.lang.Override
      public Builder clear() {
        super.clear();
        fpCount_ = 0;

        filterCosts_ = 0L;

        dbCosts_ = 0L;

        return this;
      }

      @java.lang.Override
      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return org.chainmaker.pb.txfilter.Txfilter.internal_static_txfilter_Stat_descriptor;
      }

      @java.lang.Override
      public org.chainmaker.pb.txfilter.Txfilter.Stat getDefaultInstanceForType() {
        return org.chainmaker.pb.txfilter.Txfilter.Stat.getDefaultInstance();
      }

      @java.lang.Override
      public org.chainmaker.pb.txfilter.Txfilter.Stat build() {
        org.chainmaker.pb.txfilter.Txfilter.Stat result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      @java.lang.Override
      public org.chainmaker.pb.txfilter.Txfilter.Stat buildPartial() {
        org.chainmaker.pb.txfilter.Txfilter.Stat result = new org.chainmaker.pb.txfilter.Txfilter.Stat(this);
        result.fpCount_ = fpCount_;
        result.filterCosts_ = filterCosts_;
        result.dbCosts_ = dbCosts_;
        onBuilt();
        return result;
      }

      @java.lang.Override
      public Builder clone() {
        return super.clone();
      }
      @java.lang.Override
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return super.setField(field, value);
      }
      @java.lang.Override
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return super.clearField(field);
      }
      @java.lang.Override
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return super.clearOneof(oneof);
      }
      @java.lang.Override
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, java.lang.Object value) {
        return super.setRepeatedField(field, index, value);
      }
      @java.lang.Override
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return super.addRepeatedField(field, value);
      }
      @java.lang.Override
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof org.chainmaker.pb.txfilter.Txfilter.Stat) {
          return mergeFrom((org.chainmaker.pb.txfilter.Txfilter.Stat)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(org.chainmaker.pb.txfilter.Txfilter.Stat other) {
        if (other == org.chainmaker.pb.txfilter.Txfilter.Stat.getDefaultInstance()) return this;
        if (other.getFpCount() != 0) {
          setFpCount(other.getFpCount());
        }
        if (other.getFilterCosts() != 0L) {
          setFilterCosts(other.getFilterCosts());
        }
        if (other.getDbCosts() != 0L) {
          setDbCosts(other.getDbCosts());
        }
        this.mergeUnknownFields(other.unknownFields);
        onChanged();
        return this;
      }

      @java.lang.Override
      public final boolean isInitialized() {
        return true;
      }

      @java.lang.Override
      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        org.chainmaker.pb.txfilter.Txfilter.Stat parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (org.chainmaker.pb.txfilter.Txfilter.Stat) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private int fpCount_ ;
      /**
       * <pre>
       * A value greater than 0 indicates a false positive in the tx filter, support for multiple transactions in the future
       * </pre>
       *
       * <code>uint32 fp_count = 1;</code>
       */
      public int getFpCount() {
        return fpCount_;
      }
      /**
       * <pre>
       * A value greater than 0 indicates a false positive in the tx filter, support for multiple transactions in the future
       * </pre>
       *
       * <code>uint32 fp_count = 1;</code>
       */
      public Builder setFpCount(int value) {
        
        fpCount_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       * A value greater than 0 indicates a false positive in the tx filter, support for multiple transactions in the future
       * </pre>
       *
       * <code>uint32 fp_count = 1;</code>
       */
      public Builder clearFpCount() {
        
        fpCount_ = 0;
        onChanged();
        return this;
      }

      private long filterCosts_ ;
      /**
       * <pre>
       * Filter query time
       * </pre>
       *
       * <code>int64 filter_costs = 2;</code>
       */
      public long getFilterCosts() {
        return filterCosts_;
      }
      /**
       * <pre>
       * Filter query time
       * </pre>
       *
       * <code>int64 filter_costs = 2;</code>
       */
      public Builder setFilterCosts(long value) {
        
        filterCosts_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       * Filter query time
       * </pre>
       *
       * <code>int64 filter_costs = 2;</code>
       */
      public Builder clearFilterCosts() {
        
        filterCosts_ = 0L;
        onChanged();
        return this;
      }

      private long dbCosts_ ;
      /**
       * <pre>
       * DB query time
       * </pre>
       *
       * <code>int64 db_costs = 3;</code>
       */
      public long getDbCosts() {
        return dbCosts_;
      }
      /**
       * <pre>
       * DB query time
       * </pre>
       *
       * <code>int64 db_costs = 3;</code>
       */
      public Builder setDbCosts(long value) {
        
        dbCosts_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       * DB query time
       * </pre>
       *
       * <code>int64 db_costs = 3;</code>
       */
      public Builder clearDbCosts() {
        
        dbCosts_ = 0L;
        onChanged();
        return this;
      }
      @java.lang.Override
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.setUnknownFields(unknownFields);
      }

      @java.lang.Override
      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.mergeUnknownFields(unknownFields);
      }


      // @@protoc_insertion_point(builder_scope:txfilter.Stat)
    }

    // @@protoc_insertion_point(class_scope:txfilter.Stat)
    private static final org.chainmaker.pb.txfilter.Txfilter.Stat DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new org.chainmaker.pb.txfilter.Txfilter.Stat();
    }

    public static org.chainmaker.pb.txfilter.Txfilter.Stat getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<Stat>
        PARSER = new com.google.protobuf.AbstractParser<Stat>() {
      @java.lang.Override
      public Stat parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new Stat(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<Stat> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<Stat> getParserForType() {
      return PARSER;
    }

    @java.lang.Override
    public org.chainmaker.pb.txfilter.Txfilter.Stat getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_txfilter_Stat_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_txfilter_Stat_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\027txfilter/txfilter.proto\022\010txfilter\"@\n\004S" +
      "tat\022\020\n\010fp_count\030\001 \001(\r\022\024\n\014filter_costs\030\002 " +
      "\001(\003\022\020\n\010db_costs\030\003 \001(\003BI\n\032org.chainmaker." +
      "pb.txfilterZ+chainmaker.org/chainmaker/p" +
      "b-go/v2/txfilterb\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_txfilter_Stat_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_txfilter_Stat_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_txfilter_Stat_descriptor,
        new java.lang.String[] { "FpCount", "FilterCosts", "DbCosts", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}

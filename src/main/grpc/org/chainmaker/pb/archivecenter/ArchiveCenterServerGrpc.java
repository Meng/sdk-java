package org.chainmaker.pb.archivecenter;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.25.0)",
    comments = "Source: archivecenter/archivecenter.proto")
public final class ArchiveCenterServerGrpc {

  private ArchiveCenterServerGrpc() {}

  public static final String SERVICE_NAME = "archivecenter.ArchiveCenterServer";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.RegisterResp> getRegisterMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Register",
      requestType = org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest.class,
      responseType = org.chainmaker.pb.archivecenter.Archivecenter.RegisterResp.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.RegisterResp> getRegisterMethod() {
    io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest, org.chainmaker.pb.archivecenter.Archivecenter.RegisterResp> getRegisterMethod;
    if ((getRegisterMethod = ArchiveCenterServerGrpc.getRegisterMethod) == null) {
      synchronized (ArchiveCenterServerGrpc.class) {
        if ((getRegisterMethod = ArchiveCenterServerGrpc.getRegisterMethod) == null) {
          ArchiveCenterServerGrpc.getRegisterMethod = getRegisterMethod =
              io.grpc.MethodDescriptor.<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest, org.chainmaker.pb.archivecenter.Archivecenter.RegisterResp>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "Register"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.RegisterResp.getDefaultInstance()))
              .setSchemaDescriptor(new ArchiveCenterServerMethodDescriptorSupplier("Register"))
              .build();
        }
      }
    }
    return getRegisterMethod;
  }

  private static volatile io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockResp> getArchiveBlocksMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ArchiveBlocks",
      requestType = org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest.class,
      responseType = org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockResp.class,
      methodType = io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
  public static io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockResp> getArchiveBlocksMethod() {
    io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest, org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockResp> getArchiveBlocksMethod;
    if ((getArchiveBlocksMethod = ArchiveCenterServerGrpc.getArchiveBlocksMethod) == null) {
      synchronized (ArchiveCenterServerGrpc.class) {
        if ((getArchiveBlocksMethod = ArchiveCenterServerGrpc.getArchiveBlocksMethod) == null) {
          ArchiveCenterServerGrpc.getArchiveBlocksMethod = getArchiveBlocksMethod =
              io.grpc.MethodDescriptor.<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest, org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockResp>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ArchiveBlocks"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockResp.getDefaultInstance()))
              .setSchemaDescriptor(new ArchiveCenterServerMethodDescriptorSupplier("ArchiveBlocks"))
              .build();
        }
      }
    }
    return getArchiveBlocksMethod;
  }

  private static volatile io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.SingleArchiveBlockResp> getSingleArchiveBlocksMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SingleArchiveBlocks",
      requestType = org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest.class,
      responseType = org.chainmaker.pb.archivecenter.Archivecenter.SingleArchiveBlockResp.class,
      methodType = io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
  public static io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.SingleArchiveBlockResp> getSingleArchiveBlocksMethod() {
    io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest, org.chainmaker.pb.archivecenter.Archivecenter.SingleArchiveBlockResp> getSingleArchiveBlocksMethod;
    if ((getSingleArchiveBlocksMethod = ArchiveCenterServerGrpc.getSingleArchiveBlocksMethod) == null) {
      synchronized (ArchiveCenterServerGrpc.class) {
        if ((getSingleArchiveBlocksMethod = ArchiveCenterServerGrpc.getSingleArchiveBlocksMethod) == null) {
          ArchiveCenterServerGrpc.getSingleArchiveBlocksMethod = getSingleArchiveBlocksMethod =
              io.grpc.MethodDescriptor.<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest, org.chainmaker.pb.archivecenter.Archivecenter.SingleArchiveBlockResp>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "SingleArchiveBlocks"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.SingleArchiveBlockResp.getDefaultInstance()))
              .setSchemaDescriptor(new ArchiveCenterServerMethodDescriptorSupplier("SingleArchiveBlocks"))
              .build();
        }
      }
    }
    return getSingleArchiveBlocksMethod;
  }

  private static volatile io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusResp> getGetArchivedStatusMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetArchivedStatus",
      requestType = org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest.class,
      responseType = org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusResp.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusResp> getGetArchivedStatusMethod() {
    io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest, org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusResp> getGetArchivedStatusMethod;
    if ((getGetArchivedStatusMethod = ArchiveCenterServerGrpc.getGetArchivedStatusMethod) == null) {
      synchronized (ArchiveCenterServerGrpc.class) {
        if ((getGetArchivedStatusMethod = ArchiveCenterServerGrpc.getGetArchivedStatusMethod) == null) {
          ArchiveCenterServerGrpc.getGetArchivedStatusMethod = getGetArchivedStatusMethod =
              io.grpc.MethodDescriptor.<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest, org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusResp>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetArchivedStatus"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusResp.getDefaultInstance()))
              .setSchemaDescriptor(new ArchiveCenterServerMethodDescriptorSupplier("GetArchivedStatus"))
              .build();
        }
      }
    }
    return getGetArchivedStatusMethod;
  }

  private static volatile io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.StoreStatusResp> getGetStoreStatusMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetStoreStatus",
      requestType = org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest.class,
      responseType = org.chainmaker.pb.archivecenter.Archivecenter.StoreStatusResp.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.StoreStatusResp> getGetStoreStatusMethod() {
    io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest, org.chainmaker.pb.archivecenter.Archivecenter.StoreStatusResp> getGetStoreStatusMethod;
    if ((getGetStoreStatusMethod = ArchiveCenterServerGrpc.getGetStoreStatusMethod) == null) {
      synchronized (ArchiveCenterServerGrpc.class) {
        if ((getGetStoreStatusMethod = ArchiveCenterServerGrpc.getGetStoreStatusMethod) == null) {
          ArchiveCenterServerGrpc.getGetStoreStatusMethod = getGetStoreStatusMethod =
              io.grpc.MethodDescriptor.<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest, org.chainmaker.pb.archivecenter.Archivecenter.StoreStatusResp>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetStoreStatus"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.StoreStatusResp.getDefaultInstance()))
              .setSchemaDescriptor(new ArchiveCenterServerMethodDescriptorSupplier("GetStoreStatus"))
              .build();
        }
      }
    }
    return getGetStoreStatusMethod;
  }

  private static volatile io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.RangeBlocksRequest,
      org.chainmaker.pb.common.ChainmakerBlock.BlockInfo> getGetRangeBlocksMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetRangeBlocks",
      requestType = org.chainmaker.pb.archivecenter.Archivecenter.RangeBlocksRequest.class,
      responseType = org.chainmaker.pb.common.ChainmakerBlock.BlockInfo.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.RangeBlocksRequest,
      org.chainmaker.pb.common.ChainmakerBlock.BlockInfo> getGetRangeBlocksMethod() {
    io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.RangeBlocksRequest, org.chainmaker.pb.common.ChainmakerBlock.BlockInfo> getGetRangeBlocksMethod;
    if ((getGetRangeBlocksMethod = ArchiveCenterServerGrpc.getGetRangeBlocksMethod) == null) {
      synchronized (ArchiveCenterServerGrpc.class) {
        if ((getGetRangeBlocksMethod = ArchiveCenterServerGrpc.getGetRangeBlocksMethod) == null) {
          ArchiveCenterServerGrpc.getGetRangeBlocksMethod = getGetRangeBlocksMethod =
              io.grpc.MethodDescriptor.<org.chainmaker.pb.archivecenter.Archivecenter.RangeBlocksRequest, org.chainmaker.pb.common.ChainmakerBlock.BlockInfo>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetRangeBlocks"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.RangeBlocksRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.common.ChainmakerBlock.BlockInfo.getDefaultInstance()))
              .setSchemaDescriptor(new ArchiveCenterServerMethodDescriptorSupplier("GetRangeBlocks"))
              .build();
        }
      }
    }
    return getGetRangeBlocksMethod;
  }

  private static volatile io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.BlockByHashRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getGetBlockByHashMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetBlockByHash",
      requestType = org.chainmaker.pb.archivecenter.Archivecenter.BlockByHashRequest.class,
      responseType = org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.BlockByHashRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getGetBlockByHashMethod() {
    io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.BlockByHashRequest, org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getGetBlockByHashMethod;
    if ((getGetBlockByHashMethod = ArchiveCenterServerGrpc.getGetBlockByHashMethod) == null) {
      synchronized (ArchiveCenterServerGrpc.class) {
        if ((getGetBlockByHashMethod = ArchiveCenterServerGrpc.getGetBlockByHashMethod) == null) {
          ArchiveCenterServerGrpc.getGetBlockByHashMethod = getGetBlockByHashMethod =
              io.grpc.MethodDescriptor.<org.chainmaker.pb.archivecenter.Archivecenter.BlockByHashRequest, org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetBlockByHash"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.BlockByHashRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp.getDefaultInstance()))
              .setSchemaDescriptor(new ArchiveCenterServerMethodDescriptorSupplier("GetBlockByHash"))
              .build();
        }
      }
    }
    return getGetBlockByHashMethod;
  }

  private static volatile io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.BlockByHeightRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getGetBlockByHeightMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetBlockByHeight",
      requestType = org.chainmaker.pb.archivecenter.Archivecenter.BlockByHeightRequest.class,
      responseType = org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.BlockByHeightRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getGetBlockByHeightMethod() {
    io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.BlockByHeightRequest, org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getGetBlockByHeightMethod;
    if ((getGetBlockByHeightMethod = ArchiveCenterServerGrpc.getGetBlockByHeightMethod) == null) {
      synchronized (ArchiveCenterServerGrpc.class) {
        if ((getGetBlockByHeightMethod = ArchiveCenterServerGrpc.getGetBlockByHeightMethod) == null) {
          ArchiveCenterServerGrpc.getGetBlockByHeightMethod = getGetBlockByHeightMethod =
              io.grpc.MethodDescriptor.<org.chainmaker.pb.archivecenter.Archivecenter.BlockByHeightRequest, org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetBlockByHeight"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.BlockByHeightRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp.getDefaultInstance()))
              .setSchemaDescriptor(new ArchiveCenterServerMethodDescriptorSupplier("GetBlockByHeight"))
              .build();
        }
      }
    }
    return getGetBlockByHeightMethod;
  }

  private static volatile io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getGetBlockByTxIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetBlockByTxId",
      requestType = org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest.class,
      responseType = org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getGetBlockByTxIdMethod() {
    io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest, org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getGetBlockByTxIdMethod;
    if ((getGetBlockByTxIdMethod = ArchiveCenterServerGrpc.getGetBlockByTxIdMethod) == null) {
      synchronized (ArchiveCenterServerGrpc.class) {
        if ((getGetBlockByTxIdMethod = ArchiveCenterServerGrpc.getGetBlockByTxIdMethod) == null) {
          ArchiveCenterServerGrpc.getGetBlockByTxIdMethod = getGetBlockByTxIdMethod =
              io.grpc.MethodDescriptor.<org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest, org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetBlockByTxId"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp.getDefaultInstance()))
              .setSchemaDescriptor(new ArchiveCenterServerMethodDescriptorSupplier("GetBlockByTxId"))
              .build();
        }
      }
    }
    return getGetBlockByTxIdMethod;
  }

  private static volatile io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.TxRWSetResp> getGetTxRWSetByTxIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetTxRWSetByTxId",
      requestType = org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest.class,
      responseType = org.chainmaker.pb.archivecenter.Archivecenter.TxRWSetResp.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.TxRWSetResp> getGetTxRWSetByTxIdMethod() {
    io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest, org.chainmaker.pb.archivecenter.Archivecenter.TxRWSetResp> getGetTxRWSetByTxIdMethod;
    if ((getGetTxRWSetByTxIdMethod = ArchiveCenterServerGrpc.getGetTxRWSetByTxIdMethod) == null) {
      synchronized (ArchiveCenterServerGrpc.class) {
        if ((getGetTxRWSetByTxIdMethod = ArchiveCenterServerGrpc.getGetTxRWSetByTxIdMethod) == null) {
          ArchiveCenterServerGrpc.getGetTxRWSetByTxIdMethod = getGetTxRWSetByTxIdMethod =
              io.grpc.MethodDescriptor.<org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest, org.chainmaker.pb.archivecenter.Archivecenter.TxRWSetResp>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetTxRWSetByTxId"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.TxRWSetResp.getDefaultInstance()))
              .setSchemaDescriptor(new ArchiveCenterServerMethodDescriptorSupplier("GetTxRWSetByTxId"))
              .build();
        }
      }
    }
    return getGetTxRWSetByTxIdMethod;
  }

  private static volatile io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.TransactionResp> getGetTxByTxIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetTxByTxId",
      requestType = org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest.class,
      responseType = org.chainmaker.pb.archivecenter.Archivecenter.TransactionResp.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.TransactionResp> getGetTxByTxIdMethod() {
    io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest, org.chainmaker.pb.archivecenter.Archivecenter.TransactionResp> getGetTxByTxIdMethod;
    if ((getGetTxByTxIdMethod = ArchiveCenterServerGrpc.getGetTxByTxIdMethod) == null) {
      synchronized (ArchiveCenterServerGrpc.class) {
        if ((getGetTxByTxIdMethod = ArchiveCenterServerGrpc.getGetTxByTxIdMethod) == null) {
          ArchiveCenterServerGrpc.getGetTxByTxIdMethod = getGetTxByTxIdMethod =
              io.grpc.MethodDescriptor.<org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest, org.chainmaker.pb.archivecenter.Archivecenter.TransactionResp>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetTxByTxId"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.TransactionResp.getDefaultInstance()))
              .setSchemaDescriptor(new ArchiveCenterServerMethodDescriptorSupplier("GetTxByTxId"))
              .build();
        }
      }
    }
    return getGetTxByTxIdMethod;
  }

  private static volatile io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getGetLastConfigBlockMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetLastConfigBlock",
      requestType = org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest.class,
      responseType = org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getGetLastConfigBlockMethod() {
    io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest, org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getGetLastConfigBlockMethod;
    if ((getGetLastConfigBlockMethod = ArchiveCenterServerGrpc.getGetLastConfigBlockMethod) == null) {
      synchronized (ArchiveCenterServerGrpc.class) {
        if ((getGetLastConfigBlockMethod = ArchiveCenterServerGrpc.getGetLastConfigBlockMethod) == null) {
          ArchiveCenterServerGrpc.getGetLastConfigBlockMethod = getGetLastConfigBlockMethod =
              io.grpc.MethodDescriptor.<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest, org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetLastConfigBlock"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp.getDefaultInstance()))
              .setSchemaDescriptor(new ArchiveCenterServerMethodDescriptorSupplier("GetLastConfigBlock"))
              .build();
        }
      }
    }
    return getGetLastConfigBlockMethod;
  }

  private static volatile io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdResp> getGetTxDetailByTxIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetTxDetailByTxId",
      requestType = org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdRequest.class,
      responseType = org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdResp.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdRequest,
      org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdResp> getGetTxDetailByTxIdMethod() {
    io.grpc.MethodDescriptor<org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdRequest, org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdResp> getGetTxDetailByTxIdMethod;
    if ((getGetTxDetailByTxIdMethod = ArchiveCenterServerGrpc.getGetTxDetailByTxIdMethod) == null) {
      synchronized (ArchiveCenterServerGrpc.class) {
        if ((getGetTxDetailByTxIdMethod = ArchiveCenterServerGrpc.getGetTxDetailByTxIdMethod) == null) {
          ArchiveCenterServerGrpc.getGetTxDetailByTxIdMethod = getGetTxDetailByTxIdMethod =
              io.grpc.MethodDescriptor.<org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdRequest, org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdResp>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetTxDetailByTxId"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdResp.getDefaultInstance()))
              .setSchemaDescriptor(new ArchiveCenterServerMethodDescriptorSupplier("GetTxDetailByTxId"))
              .build();
        }
      }
    }
    return getGetTxDetailByTxIdMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ArchiveCenterServerStub newStub(io.grpc.Channel channel) {
    return new ArchiveCenterServerStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ArchiveCenterServerBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ArchiveCenterServerBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ArchiveCenterServerFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ArchiveCenterServerFutureStub(channel);
  }

  /**
   */
  public static abstract class ArchiveCenterServerImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * 注册接口   
     * </pre>
     */
    public void register(org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.RegisterResp> responseObserver) {
      asyncUnimplementedUnaryCall(getRegisterMethod(), responseObserver);
    }

    /**
     * <pre>
     * 归档区块接口
     * </pre>
     */
    public io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest> archiveBlocks(
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockResp> responseObserver) {
      return asyncUnimplementedStreamingCall(getArchiveBlocksMethod(), responseObserver);
    }

    /**
     * <pre>
     * 归档区块接口(单向流)
     * </pre>
     */
    public io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest> singleArchiveBlocks(
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.SingleArchiveBlockResp> responseObserver) {
      return asyncUnimplementedStreamingCall(getSingleArchiveBlocksMethod(), responseObserver);
    }

    /**
     * <pre>
     * 查询归档状态
     * </pre>
     */
    public void getArchivedStatus(org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusResp> responseObserver) {
      asyncUnimplementedUnaryCall(getGetArchivedStatusMethod(), responseObserver);
    }

    /**
     * <pre>
     * 获取当前归档中心存储信息
     * </pre>
     */
    public void getStoreStatus(org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.StoreStatusResp> responseObserver) {
      asyncUnimplementedUnaryCall(getGetStoreStatusMethod(), responseObserver);
    }

    /**
     * <pre>
     * 查询高度范围内区块  
     * </pre>
     */
    public void getRangeBlocks(org.chainmaker.pb.archivecenter.Archivecenter.RangeBlocksRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.common.ChainmakerBlock.BlockInfo> responseObserver) {
      asyncUnimplementedUnaryCall(getGetRangeBlocksMethod(), responseObserver);
    }

    /**
     * <pre>
     * 根据blockhash查询区块 
     * </pre>
     */
    public void getBlockByHash(org.chainmaker.pb.archivecenter.Archivecenter.BlockByHashRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> responseObserver) {
      asyncUnimplementedUnaryCall(getGetBlockByHashMethod(), responseObserver);
    }

    /**
     * <pre>
     * 根据块高查询区块  
     * </pre>
     */
    public void getBlockByHeight(org.chainmaker.pb.archivecenter.Archivecenter.BlockByHeightRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> responseObserver) {
      asyncUnimplementedUnaryCall(getGetBlockByHeightMethod(), responseObserver);
    }

    /**
     * <pre>
     * 根据txid查询区块  
     * </pre>
     */
    public void getBlockByTxId(org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> responseObserver) {
      asyncUnimplementedUnaryCall(getGetBlockByTxIdMethod(), responseObserver);
    }

    /**
     * <pre>
     * 根据txid查询RWSET 
     * </pre>
     */
    public void getTxRWSetByTxId(org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.TxRWSetResp> responseObserver) {
      asyncUnimplementedUnaryCall(getGetTxRWSetByTxIdMethod(), responseObserver);
    }

    /**
     * <pre>
     * 根据txid查询transaction  
     * </pre>
     */
    public void getTxByTxId(org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.TransactionResp> responseObserver) {
      asyncUnimplementedUnaryCall(getGetTxByTxIdMethod(), responseObserver);
    }

    /**
     * <pre>
     * 查询归档中心的最新的配置区块信息 
     * </pre>
     */
    public void getLastConfigBlock(org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> responseObserver) {
      asyncUnimplementedUnaryCall(getGetLastConfigBlockMethod(), responseObserver);
    }

    /**
     * <pre>
     * 根据txid查询块高，或判定tx是否存在，或查询tx的confirmtime 
     * </pre>
     */
    public void getTxDetailByTxId(org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdResp> responseObserver) {
      asyncUnimplementedUnaryCall(getGetTxDetailByTxIdMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getRegisterMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest,
                org.chainmaker.pb.archivecenter.Archivecenter.RegisterResp>(
                  this, METHODID_REGISTER)))
          .addMethod(
            getArchiveBlocksMethod(),
            asyncBidiStreamingCall(
              new MethodHandlers<
                org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest,
                org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockResp>(
                  this, METHODID_ARCHIVE_BLOCKS)))
          .addMethod(
            getSingleArchiveBlocksMethod(),
            asyncClientStreamingCall(
              new MethodHandlers<
                org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest,
                org.chainmaker.pb.archivecenter.Archivecenter.SingleArchiveBlockResp>(
                  this, METHODID_SINGLE_ARCHIVE_BLOCKS)))
          .addMethod(
            getGetArchivedStatusMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest,
                org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusResp>(
                  this, METHODID_GET_ARCHIVED_STATUS)))
          .addMethod(
            getGetStoreStatusMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest,
                org.chainmaker.pb.archivecenter.Archivecenter.StoreStatusResp>(
                  this, METHODID_GET_STORE_STATUS)))
          .addMethod(
            getGetRangeBlocksMethod(),
            asyncServerStreamingCall(
              new MethodHandlers<
                org.chainmaker.pb.archivecenter.Archivecenter.RangeBlocksRequest,
                org.chainmaker.pb.common.ChainmakerBlock.BlockInfo>(
                  this, METHODID_GET_RANGE_BLOCKS)))
          .addMethod(
            getGetBlockByHashMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.chainmaker.pb.archivecenter.Archivecenter.BlockByHashRequest,
                org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp>(
                  this, METHODID_GET_BLOCK_BY_HASH)))
          .addMethod(
            getGetBlockByHeightMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.chainmaker.pb.archivecenter.Archivecenter.BlockByHeightRequest,
                org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp>(
                  this, METHODID_GET_BLOCK_BY_HEIGHT)))
          .addMethod(
            getGetBlockByTxIdMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest,
                org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp>(
                  this, METHODID_GET_BLOCK_BY_TX_ID)))
          .addMethod(
            getGetTxRWSetByTxIdMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest,
                org.chainmaker.pb.archivecenter.Archivecenter.TxRWSetResp>(
                  this, METHODID_GET_TX_RWSET_BY_TX_ID)))
          .addMethod(
            getGetTxByTxIdMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest,
                org.chainmaker.pb.archivecenter.Archivecenter.TransactionResp>(
                  this, METHODID_GET_TX_BY_TX_ID)))
          .addMethod(
            getGetLastConfigBlockMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest,
                org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp>(
                  this, METHODID_GET_LAST_CONFIG_BLOCK)))
          .addMethod(
            getGetTxDetailByTxIdMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdRequest,
                org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdResp>(
                  this, METHODID_GET_TX_DETAIL_BY_TX_ID)))
          .build();
    }
  }

  /**
   */
  public static final class ArchiveCenterServerStub extends io.grpc.stub.AbstractStub<ArchiveCenterServerStub> {
    private ArchiveCenterServerStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ArchiveCenterServerStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ArchiveCenterServerStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ArchiveCenterServerStub(channel, callOptions);
    }

    /**
     * <pre>
     * 注册接口   
     * </pre>
     */
    public void register(org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.RegisterResp> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getRegisterMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * 归档区块接口
     * </pre>
     */
    public io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest> archiveBlocks(
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockResp> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(getArchiveBlocksMethod(), getCallOptions()), responseObserver);
    }

    /**
     * <pre>
     * 归档区块接口(单向流)
     * </pre>
     */
    public io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest> singleArchiveBlocks(
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.SingleArchiveBlockResp> responseObserver) {
      return asyncClientStreamingCall(
          getChannel().newCall(getSingleArchiveBlocksMethod(), getCallOptions()), responseObserver);
    }

    /**
     * <pre>
     * 查询归档状态
     * </pre>
     */
    public void getArchivedStatus(org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusResp> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetArchivedStatusMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * 获取当前归档中心存储信息
     * </pre>
     */
    public void getStoreStatus(org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.StoreStatusResp> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetStoreStatusMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * 查询高度范围内区块  
     * </pre>
     */
    public void getRangeBlocks(org.chainmaker.pb.archivecenter.Archivecenter.RangeBlocksRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.common.ChainmakerBlock.BlockInfo> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getGetRangeBlocksMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * 根据blockhash查询区块 
     * </pre>
     */
    public void getBlockByHash(org.chainmaker.pb.archivecenter.Archivecenter.BlockByHashRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetBlockByHashMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * 根据块高查询区块  
     * </pre>
     */
    public void getBlockByHeight(org.chainmaker.pb.archivecenter.Archivecenter.BlockByHeightRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetBlockByHeightMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * 根据txid查询区块  
     * </pre>
     */
    public void getBlockByTxId(org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetBlockByTxIdMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * 根据txid查询RWSET 
     * </pre>
     */
    public void getTxRWSetByTxId(org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.TxRWSetResp> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetTxRWSetByTxIdMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * 根据txid查询transaction  
     * </pre>
     */
    public void getTxByTxId(org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.TransactionResp> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetTxByTxIdMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * 查询归档中心的最新的配置区块信息 
     * </pre>
     */
    public void getLastConfigBlock(org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetLastConfigBlockMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * 根据txid查询块高，或判定tx是否存在，或查询tx的confirmtime 
     * </pre>
     */
    public void getTxDetailByTxId(org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdRequest request,
        io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdResp> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetTxDetailByTxIdMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class ArchiveCenterServerBlockingStub extends io.grpc.stub.AbstractStub<ArchiveCenterServerBlockingStub> {
    private ArchiveCenterServerBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ArchiveCenterServerBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ArchiveCenterServerBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ArchiveCenterServerBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * 注册接口   
     * </pre>
     */
    public org.chainmaker.pb.archivecenter.Archivecenter.RegisterResp register(org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest request) {
      return blockingUnaryCall(
          getChannel(), getRegisterMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * 查询归档状态
     * </pre>
     */
    public org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusResp getArchivedStatus(org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetArchivedStatusMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * 获取当前归档中心存储信息
     * </pre>
     */
    public org.chainmaker.pb.archivecenter.Archivecenter.StoreStatusResp getStoreStatus(org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetStoreStatusMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * 查询高度范围内区块  
     * </pre>
     */
    public java.util.Iterator<org.chainmaker.pb.common.ChainmakerBlock.BlockInfo> getRangeBlocks(
        org.chainmaker.pb.archivecenter.Archivecenter.RangeBlocksRequest request) {
      return blockingServerStreamingCall(
          getChannel(), getGetRangeBlocksMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * 根据blockhash查询区块 
     * </pre>
     */
    public org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp getBlockByHash(org.chainmaker.pb.archivecenter.Archivecenter.BlockByHashRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetBlockByHashMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * 根据块高查询区块  
     * </pre>
     */
    public org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp getBlockByHeight(org.chainmaker.pb.archivecenter.Archivecenter.BlockByHeightRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetBlockByHeightMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * 根据txid查询区块  
     * </pre>
     */
    public org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp getBlockByTxId(org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetBlockByTxIdMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * 根据txid查询RWSET 
     * </pre>
     */
    public org.chainmaker.pb.archivecenter.Archivecenter.TxRWSetResp getTxRWSetByTxId(org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetTxRWSetByTxIdMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * 根据txid查询transaction  
     * </pre>
     */
    public org.chainmaker.pb.archivecenter.Archivecenter.TransactionResp getTxByTxId(org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetTxByTxIdMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * 查询归档中心的最新的配置区块信息 
     * </pre>
     */
    public org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp getLastConfigBlock(org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetLastConfigBlockMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * 根据txid查询块高，或判定tx是否存在，或查询tx的confirmtime 
     * </pre>
     */
    public org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdResp getTxDetailByTxId(org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetTxDetailByTxIdMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class ArchiveCenterServerFutureStub extends io.grpc.stub.AbstractStub<ArchiveCenterServerFutureStub> {
    private ArchiveCenterServerFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ArchiveCenterServerFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ArchiveCenterServerFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ArchiveCenterServerFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * 注册接口   
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<org.chainmaker.pb.archivecenter.Archivecenter.RegisterResp> register(
        org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getRegisterMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * 查询归档状态
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusResp> getArchivedStatus(
        org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetArchivedStatusMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * 获取当前归档中心存储信息
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<org.chainmaker.pb.archivecenter.Archivecenter.StoreStatusResp> getStoreStatus(
        org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetStoreStatusMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * 根据blockhash查询区块 
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getBlockByHash(
        org.chainmaker.pb.archivecenter.Archivecenter.BlockByHashRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetBlockByHashMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * 根据块高查询区块  
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getBlockByHeight(
        org.chainmaker.pb.archivecenter.Archivecenter.BlockByHeightRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetBlockByHeightMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * 根据txid查询区块  
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getBlockByTxId(
        org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetBlockByTxIdMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * 根据txid查询RWSET 
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<org.chainmaker.pb.archivecenter.Archivecenter.TxRWSetResp> getTxRWSetByTxId(
        org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetTxRWSetByTxIdMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * 根据txid查询transaction  
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<org.chainmaker.pb.archivecenter.Archivecenter.TransactionResp> getTxByTxId(
        org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetTxByTxIdMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * 查询归档中心的最新的配置区块信息 
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp> getLastConfigBlock(
        org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetLastConfigBlockMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * 根据txid查询块高，或判定tx是否存在，或查询tx的confirmtime 
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdResp> getTxDetailByTxId(
        org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetTxDetailByTxIdMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_REGISTER = 0;
  private static final int METHODID_GET_ARCHIVED_STATUS = 1;
  private static final int METHODID_GET_STORE_STATUS = 2;
  private static final int METHODID_GET_RANGE_BLOCKS = 3;
  private static final int METHODID_GET_BLOCK_BY_HASH = 4;
  private static final int METHODID_GET_BLOCK_BY_HEIGHT = 5;
  private static final int METHODID_GET_BLOCK_BY_TX_ID = 6;
  private static final int METHODID_GET_TX_RWSET_BY_TX_ID = 7;
  private static final int METHODID_GET_TX_BY_TX_ID = 8;
  private static final int METHODID_GET_LAST_CONFIG_BLOCK = 9;
  private static final int METHODID_GET_TX_DETAIL_BY_TX_ID = 10;
  private static final int METHODID_ARCHIVE_BLOCKS = 11;
  private static final int METHODID_SINGLE_ARCHIVE_BLOCKS = 12;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ArchiveCenterServerImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ArchiveCenterServerImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_REGISTER:
          serviceImpl.register((org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockRequest) request,
              (io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.RegisterResp>) responseObserver);
          break;
        case METHODID_GET_ARCHIVED_STATUS:
          serviceImpl.getArchivedStatus((org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest) request,
              (io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusResp>) responseObserver);
          break;
        case METHODID_GET_STORE_STATUS:
          serviceImpl.getStoreStatus((org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest) request,
              (io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.StoreStatusResp>) responseObserver);
          break;
        case METHODID_GET_RANGE_BLOCKS:
          serviceImpl.getRangeBlocks((org.chainmaker.pb.archivecenter.Archivecenter.RangeBlocksRequest) request,
              (io.grpc.stub.StreamObserver<org.chainmaker.pb.common.ChainmakerBlock.BlockInfo>) responseObserver);
          break;
        case METHODID_GET_BLOCK_BY_HASH:
          serviceImpl.getBlockByHash((org.chainmaker.pb.archivecenter.Archivecenter.BlockByHashRequest) request,
              (io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp>) responseObserver);
          break;
        case METHODID_GET_BLOCK_BY_HEIGHT:
          serviceImpl.getBlockByHeight((org.chainmaker.pb.archivecenter.Archivecenter.BlockByHeightRequest) request,
              (io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp>) responseObserver);
          break;
        case METHODID_GET_BLOCK_BY_TX_ID:
          serviceImpl.getBlockByTxId((org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest) request,
              (io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp>) responseObserver);
          break;
        case METHODID_GET_TX_RWSET_BY_TX_ID:
          serviceImpl.getTxRWSetByTxId((org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest) request,
              (io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.TxRWSetResp>) responseObserver);
          break;
        case METHODID_GET_TX_BY_TX_ID:
          serviceImpl.getTxByTxId((org.chainmaker.pb.archivecenter.Archivecenter.BlockByTxIdRequest) request,
              (io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.TransactionResp>) responseObserver);
          break;
        case METHODID_GET_LAST_CONFIG_BLOCK:
          serviceImpl.getLastConfigBlock((org.chainmaker.pb.archivecenter.Archivecenter.ArchiveStatusRequest) request,
              (io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.BlockWithRWSetResp>) responseObserver);
          break;
        case METHODID_GET_TX_DETAIL_BY_TX_ID:
          serviceImpl.getTxDetailByTxId((org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdRequest) request,
              (io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.TxDetailByIdResp>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_ARCHIVE_BLOCKS:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.archiveBlocks(
              (io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.ArchiveBlockResp>) responseObserver);
        case METHODID_SINGLE_ARCHIVE_BLOCKS:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.singleArchiveBlocks(
              (io.grpc.stub.StreamObserver<org.chainmaker.pb.archivecenter.Archivecenter.SingleArchiveBlockResp>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ArchiveCenterServerBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ArchiveCenterServerBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return org.chainmaker.pb.archivecenter.Archivecenter.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ArchiveCenterServer");
    }
  }

  private static final class ArchiveCenterServerFileDescriptorSupplier
      extends ArchiveCenterServerBaseDescriptorSupplier {
    ArchiveCenterServerFileDescriptorSupplier() {}
  }

  private static final class ArchiveCenterServerMethodDescriptorSupplier
      extends ArchiveCenterServerBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ArchiveCenterServerMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ArchiveCenterServerGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ArchiveCenterServerFileDescriptorSupplier())
              .addMethod(getRegisterMethod())
              .addMethod(getArchiveBlocksMethod())
              .addMethod(getSingleArchiveBlocksMethod())
              .addMethod(getGetArchivedStatusMethod())
              .addMethod(getGetStoreStatusMethod())
              .addMethod(getGetRangeBlocksMethod())
              .addMethod(getGetBlockByHashMethod())
              .addMethod(getGetBlockByHeightMethod())
              .addMethod(getGetBlockByTxIdMethod())
              .addMethod(getGetTxRWSetByTxIdMethod())
              .addMethod(getGetTxByTxIdMethod())
              .addMethod(getGetLastConfigBlockMethod())
              .addMethod(getGetTxDetailByTxIdMethod())
              .build();
        }
      }
    }
    return result;
  }
}

# Maven仓库地址
maven
```
<dependency>
    <groupId>org.chainmaker</groupId>
    <artifactId>chainmaker-sdk-java</artifactId>
    <version>2.3.2</version>
</dependency>
```
gradle
```
    // https://mvnrepository.com/artifact/org.chainmaker/chainmaker-sdk-java
    implementation 'org.chainmaker:chainmaker-sdk-java:2.3.2'
```
# 使用demo

如需使用sdk jar包demo，请参考<a href="https://git.chainmaker.org.cn/chainmaker/sdk-java-demo"  target="_blank"> sdk-java-demo </a>

# 适配版本一览表

| chainmaker-go | chainmaker-sdk-java                                                                                            | 
|:--------------|:---------------------------------------------------------------------------------------------------------------| 
| v1.0.0        | [v1.0.0](https://git.chainmaker.org.cn/chainmaker/chainmaker-sdk-java/-/tree/v1.0.0)                           | 
| v1.1.0        | [v1.1.0](https://git.chainmaker.org.cn/chainmaker/chainmaker-sdk-java/-/tree/v1.1.0)                           | 
| v1.1.1        | [v1.1.0](https://git.chainmaker.org.cn/chainmaker/chainmaker-sdk-java/-/tree/v1.1.0)                           | 
| v1.2.0        | [v1.2.0](https://git.chainmaker.org.cn/chainmaker/chainmaker-sdk-java/-/tree/v1.2.0)                           | 
| v1.2.3        | [v1.2.3](https://git.chainmaker.org.cn/chainmaker/chainmaker-sdk-java/-/tree/v1.2.3)                           | 
| v1.2.4        | [v1.2.4](https://git.chainmaker.org.cn/chainmaker/chainmaker-sdk-java/-/tree/v1.2.4)                           | 
| v2.0.0        | [v2.0.0](https://git.chainmaker.org.cn/chainmaker/sdk-java/-/tree/v2.0.0)                                      | 
| v2.1.0        | [v2.1.0](https://git.chainmaker.org.cn/chainmaker/sdk-java/-/tree/v2.1.0)                                      | 
| v2.2.0        | [v2.2.0](https://git.chainmaker.org.cn/chainmaker/sdk-java/-/tree/v2.2.0),v2.1.0                               | 
| v2.2.1        | [v2.2.1](https://git.chainmaker.org.cn/chainmaker/sdk-java/-/tree/v2.2.1),v2.2.0,v2.1.0                        | 
| v2.3.0        | [v2.3.0](https://git.chainmaker.org.cn/chainmaker/sdk-java/-/tree/v2.3.0),v2.2.1,v2.2.0,v2.1.0                 | 
| v2.3.1        | [v2.3.1.3](https://git.chainmaker.org.cn/chainmaker/sdk-java/-/tree/v2.3.1.3),v2.3.0,v2.2.1,v2.2.0,v2.1.0      | 
| v2.3.2        | [v2.3.2](https://git.chainmaker.org.cn/chainmaker/sdk-java/-/tree/v2.3.2),v2.3.1.3,v2.3.0,v2.2.1,v2.2.0,v2.1.0 | 
